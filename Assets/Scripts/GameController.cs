﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
	public static GameController Instance;

	[SerializeField] private Text timer;
	[SerializeField] private Text score;

	private int scoreValue = 0;

	void Awake() {
		Instance = this;
	}

	void Update() {
		timer.text = (301 - Mathf.Round(Time.realtimeSinceStartup)) + "";
	}

	public void IncrementScore() {
		++scoreValue;
		score.text = "Score: " + scoreValue;
	}

	public void RestartGame() {
		SceneManager.LoadScene (0);
	}
}
