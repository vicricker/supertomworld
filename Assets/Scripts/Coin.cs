﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	void Update () {
		transform.Rotate(0,0,50*Time.deltaTime);
	}

	void OnTriggerEnter(Collider other) {
		Destroy (this.gameObject);
		GameController.Instance.IncrementScore ();
	}
}
